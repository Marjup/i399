import {Pipe, PipeTransform} from "@angular/core";

@Pipe({
    name: "search"
})

export class SearchPipe implements PipeTransform{
    transform(contacts: any, searchString: any): any {
        if (searchString === undefined) return contacts;

        return contacts.filter(function (contact) {
            if (contact.name !== undefined) {
                if (contact.phone !== undefined) {
                    return contact.name.toLowerCase().indexOf(searchString.toLowerCase()) >= 0
                        || contact.phone.toLowerCase().indexOf(searchString.toLowerCase()) >= 0;
                } else {
                    return contact.name.toLowerCase().indexOf(searchString.toLowerCase()) >= 0;
                }
            } else {
                if (contact.phone !== undefined){
                    return contact.phone.toLowerCase().indexOf(searchString.toLowerCase()) >= 0;
                }
            }

        });
    }
}