import {Routes} from "@angular/router";
import {SearchComponent} from "./search/search.cmp";
import {NewComponent} from "./new/new.cmp";
import {EditComponent} from "./edit/edit.cmp";

export const routes: Routes = [
    { path: 'search', component: SearchComponent },
    { path: 'new', component: NewComponent },
    { path: 'edit/:id', component: EditComponent },
    { path: '', redirectTo: 'search', pathMatch: 'full' }
];
