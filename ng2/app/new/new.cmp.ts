import {Component} from "@angular/core";
import {ContactService, Contact} from "../contact.srv";
import {Router} from "@angular/router";

@Component({
    selector: 'new',
    templateUrl: 'app/new/new.html',
    styleUrls: ['./styles.css']
})

export class NewComponent {
    contactName: string;
    contactPhone: string;

    constructor(private contactService: ContactService,  private router: Router,) {}

    addNewContact(): void {
        this.contactService.saveContact(new Contact(this.contactName, this.contactPhone))
            .then(() => {
                this.router.navigateByUrl('/search');
            });
    }
}