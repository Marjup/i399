import {Component, OnInit} from "@angular/core";
import {Contact, ContactService} from "../contact.srv";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    templateUrl: 'app/edit/edit.html'
})

export class EditComponent implements OnInit {

    contact: Contact;
    newContact: Contact;


    constructor(private route: ActivatedRoute,
                private router: Router,
                private contactService: ContactService) {}

    ngOnInit(): void {
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.contactService.getContact(id)
            .then(contact => this.contact = contact);
    }

    updateContact(): void {
        const id = parseInt(this.route.snapshot.paramMap.get('id'));
        this.newContact = new Contact (this.contact.name, this.contact.phone);
        this.newContact._id = id;
        this.contactService.updateContact(this.newContact, id)
            .then(() => {
                this.router.navigateByUrl('/search');
            });
    }




}
