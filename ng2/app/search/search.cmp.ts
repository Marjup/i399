import {ContactService, Contact} from "../contact.srv";
import {Component, OnInit} from "@angular/core";

@Component({
    selector: 'search',
    templateUrl: 'app/search/search.html',
    styleUrls: ['./styles.css']
})


export class SearchComponent implements OnInit {
    contacts: Contact[] = [];
    searchString: string = "";
    ids: number[];

    constructor(private contactService: ContactService) {}

    private updateContacts(): void {
        this.contactService.getContacts().then(contacts => this.contacts = contacts);
    }

    ngOnInit(): void {
        this.updateContacts();
    }

    deleteContact(contactId : number): void {
        this.contactService.deleteContact(contactId)
            .then(() => this.updateContacts());
    }

    removeContacts(): void {
        this.ids = this.contacts.filter(function (val){
            return val.selected;
        }).map(function (val){
            return val._id;
        });
        for (let value of this.ids) {
            this.deleteContact(value);
        }
    }

}
