import {AppComponent} from "./app.cmp";
import {routes} from "./routes";
import {ContactService} from "./contact.srv";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import {SearchComponent} from "./search/search.cmp";
import {NewComponent} from "./new/new.cmp";
import {EditComponent} from "./edit/edit.cmp";
import {SearchPipe} from "./search-pipe";

@NgModule({
    imports: [ BrowserModule, HttpModule, FormsModule,
        RouterModule.forRoot(routes, { useHash: true })],
    declarations: [ AppComponent, SearchComponent, NewComponent, EditComponent, SearchPipe ],
    providers: [ ContactService ],
    bootstrap: [ AppComponent ]
})
export class AppModule { }
