import {Component} from "@angular/core";

@Component({
    selector: 'my-app',
    templateUrl: 'app/app.html',
    styleUrls: ['./styles.css']
})

export class AppComponent {
}
