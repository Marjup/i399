'use strict';

const express = require('express');
var bodyParser = require('body-parser');
const app = express();
const Dao = require('./dao.js');

app.use(express.static('./'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.get('/api/contacts', getContacts);
app.get('/api/contacts/:id', getContact);
app.post('/api/contacts', insertContact);
app.put('/api/contacts/:id', updateContact);
app.delete('/api/contacts/:id', deleteContact);
app.post('/api/contacts/delete', deleteContacts);

var url = 'mongodb://user1:123456@ds159208.mlab.com:59208/i399';
var dao = new Dao();

dao.connect(url)
    .then(() => app.listen(3000));

function deleteContacts(request, response) {
    var contacts = request.body;
    response.set('Content-Type', 'application/json');
    dao.deleteMany(contacts).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}

function deleteContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    dao.deleteContact(id).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}

function updateContact(request, response) {
    var id = request.body._id;
    var contact = request.body;
    response.set('Content-Type', 'application/json');
    dao.update(id, contact).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}

function insertContact(request, response) {
    var contact = request.body;
    response.set('Content-Type', 'application/json');
    dao.insert(contact).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}

function getContacts(request, response) {
    response.set('Content-Type', 'application/json');
    dao.findAll().then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}

function getContact(request, response) {
    var id = request.params.id;
    response.set('Content-Type', 'application/json');
    dao.findById(id).then(data => {
        response.end(JSON.stringify(data));
    }).catch(error => {
        console.log(error);
        response.end('error' + error);
    });
}
