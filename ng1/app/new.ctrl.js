(function () {
    'use strict';

    angular.module('app').controller('NewCtrl', Ctrl);

    function Ctrl($http, $location){
        var vm = this;

        this.newContact = {
            name: '',
            phone: ''
        };
        this.submitContact = addContact;

        function addContact() {
            var newContact = {
                name: vm.newContact.name,
                phone: vm.newContact.phone
            };

            $http.post('api/contacts', newContact).then(function (){
                $location.path('/search');
            });
        }
    }

}) ();
