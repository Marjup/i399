(function () {
    'use strict';

    angular.module('app').controller('SearchCtrl', Ctrl);

    function Ctrl($http, modalService){
        var vm = this;

        this.contacts = [];
        this.searchString = "";
        this.removeContact = removeContact;
        this.removeContacts = removeContacts;
        this.filterContactList = filterContactList;

        init();

        function init() {
            $http.get('api/contacts').then(function (result){
                vm.contacts = result.data;
            });
        }

        function removeContacts() {
            var selectedContacts = vm.contacts.filter(function (val) {
                return val.selected;
            }).map(function (val) {
                return val._id;
            });
            modalService.confirm().then(function(){
                return $http.post('api/contacts/delete', selectedContacts);
            }).then(init);
        }

        function removeContact(id) {
            modalService.confirm().then(function(){
                return $http.delete('api/contacts/' + id);
            }).then(init);
        }

        function filterContactList(contact) {
            return 0 === vm.searchString.length || contains(contact.name) || contains(contact.phone);
        }

        function contains(str) {
            return void 0 !== str && str.toLowerCase().indexOf(vm.searchString.toLowerCase()) >= 0
        }
    }

}) ();
