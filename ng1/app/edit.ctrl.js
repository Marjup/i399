(function () {
    'use strict';

    angular.module('app').controller('EditCtrl', Ctrl);

    function Ctrl($http, $routeParams, $location){
        var vm = this;

        this.newContact = {};
        this.submitContact = editContact;

        $http.get('api/contacts/' + $routeParams.id).then(function (result){
            vm.newContact = result.data;
        });

        function editContact() {
            var newContact = {
                name: vm.newContact.name,
                phone: vm.newContact.phone,
                _id: $routeParams.id
            };

            $http.put('api/contacts/' + $routeParams.id, newContact).then(function (){
                $location.path('/search');
            });
        }
    }

}) ();